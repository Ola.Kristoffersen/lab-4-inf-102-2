package INF102.lab4.median;

import java.util.ArrayList;
import java.util.List;

public class QuickMedian implements IMedian {
    @Override
    public <T extends Comparable<T>> T median(List<T> list) {
        List<T> listCopy = new ArrayList<>(list); // Method should not alter list
        
        int len = listCopy.size() ;
        int k = len / 2;
        return quickMed(listCopy,0,len-1,k);
    }
    private <T extends Comparable<T>> T quickMed(List<T> list, int low, int high, int k){

        int p = partition(list, low, high);
        
        if(low == high){
            return list.get(low);
        }
        if(k == p){
            return list.get(p);
        }
        else if(k < p){
            return quickMed(list, low, p - 1, k);
        }
        else {
            return quickMed(list, p + 1, high, k);
        }
        
    }

    private <T extends Comparable<T>> int partition(List<T> list, int low, int high) {

        T pivot = list.get(high);
        int z = low - 1;

        for(int i = low; i < high; i++){
            if(list.get(i).compareTo(pivot)<0){
                z++;
                T temp = list.get(z);
                list.set(z, list.get(i));
                list.set(i, temp);
            }
        }
        T current = list.get(z+1);
        list.set(z + 1, list.get(high));
        list.set(high, current);
        return z + 1;
    }

}

