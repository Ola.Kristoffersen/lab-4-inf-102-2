package INF102.lab4.sorting;

import java.util.List;

public class BubbleSort implements ISort {

    @Override
    public <T extends Comparable<T>> void sort(List<T> list) {

        int n = list.size();
        boolean swapped;
        for (int i = 0; i < n - 1; i++) {
            swapped = false;
            for (int j = 0 ; j < n-1 ; j++) {
                if (list.get(j).compareTo(list.get(j+1))>0){
                T temp = list.get(i);
                int a = j;
                int b = j+1;
                list.set(a, list.get(i+1));
                list.set(b, temp);
                swapped = true;
            }


            }
        
        }
    
}
}
